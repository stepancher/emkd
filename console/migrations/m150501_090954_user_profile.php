<?php

use yii\db\Schema;
use yii\db\Migration;

class m150501_090954_user_profile extends Migration
{
    public function up()
    {
	    $this->createTable('{{%user_profile}}', [
		    'id_user' => Schema::TYPE_INTEGER . ' UNIQUE',
		    'firstname' => Schema::TYPE_STRING . '(100) NOT NULL',
		    'middlename' => Schema::TYPE_STRING . '(100)',
		    'lastname' => Schema::TYPE_STRING . '(100)',
            'date_birthday' => Schema::TYPE_DATE,
            'avatar' => Schema::TYPE_STRING . '(255)',
            'sex' => Schema::TYPE_BOOLEAN. ' DEFAULT TRUE',
            'skype' => Schema::TYPE_STRING . '(255)',
            'phone_number' => Schema::TYPE_STRING . '(255)',
            'icq' => Schema::TYPE_STRING . '(255)',
            'notes' => Schema::TYPE_TEXT,

        ]);
	    $this->addForeignKey('user_profile_fk', '{{%user_profile}}', 'id_user', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
	    $this->dropTable('{{%user_profile}}');
    }
}
