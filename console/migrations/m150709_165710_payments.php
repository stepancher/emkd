<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_165710_payments extends Migration
{
    public function safeUp()
    {

        $this->createTable('{{%payments}}', [
            'id' => Schema::TYPE_PK,
            'id_user'=>Schema::TYPE_INTEGER,

            'date_add'=>Schema::TYPE_TIMESTAMP,
            'date_pay'=>Schema::TYPE_TIMESTAMP,
            'date_enrolled'=>Schema::TYPE_TIMESTAMP,
            'type'=>Schema::TYPE_TEXT,
            'num_bank'=>Schema::TYPE_TEXT,
            'summ' => Schema::TYPE_DECIMAL . '(10,2) NOT NULL DEFAULT 0',
            'payment_info1'=>Schema::TYPE_TEXT,
            'payment_info2'=>Schema::TYPE_TEXT,
            'payment_info3'=>Schema::TYPE_TEXT,
            'payment_info4'=>Schema::TYPE_TEXT,
            'status'=>Schema::TYPE_SMALLINT,
            'acc_for' => Schema::TYPE_STRING . '(12) NOT NULL',
            'hash' => Schema::TYPE_TEXT . ' NOT NULL',

            'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL'
        ], 'ENGINE=MyISAM AUTO_INCREMENT=25155 DEFAULT CHARSET=utf8 COLLATE=utf8_bin');
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%payments}}');
    }

}
