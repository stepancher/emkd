<?php

use yii\db\Schema;
use yii\db\Migration;

class m150502_113940_admin_add extends Migration
{
    public function safeUp()
    {
        $admin = new \common\models\User();
        $admin->username = 'admin';
        $admin->email = 'admin@example.ru';

        $admin->setPassword('admin123');
        $admin->generateAuthKey();
        $admin->setActiveStatus();
        if($admin->save()) {
            echo "Пользователь Админ создан";
            $userRole = Yii::$app->authManager->getRole('admin');
            Yii::$app->authManager->assign($userRole, $admin->getId());
            echo "Роль админ установлена";
            $userProfile = new \common\models\UserProfile();
            $userProfile->id_user = $admin->id;
            $userProfile->firstname = 'Админ';
            $userProfile->lastname = 'Админ';
            $userProfile->save();
            return true;
        }else{
            echo "Админ не был создан. Ошибка";
            return false;
        }
    }
    
    public function safeDown()
    {
        /**@var $admin common\models\User*/
        $admin = \common\models\User::findByEmail('admin@example.ru');
        if($admin){
            $admin->delete();
            echo "Админ был успешно удален";
        }else{
            echo "Админ не существует";
        }
        return true;
    }
}
