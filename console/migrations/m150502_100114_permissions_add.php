<?php

use yii\db\Schema;
use yii\db\Migration;

class m150502_100114_permissions_add extends Migration
{
    private $permissions = [
        'r_main'=>'Просмотр главной страницы админки',
        'w_main'=>'Редактирование главной страницы админки',
        'backend'=>'Доступ в админ панель',
        'r_payments'=>'Просмотр платежей в админке',
        'w_payments'=>'Изменение платежей в админке',

    ];
    private $permissionsAdmin = [
        'r_user'=>'Просмотр пользователей',
        'w_user'=>'Изменение пользователей',
        'r_role'=>'Просмотр ролей и прав',
        'w_role'=>'Изменение ролей и прав',
    ];

    public function safeUp()
    {
        //add roles and permissions
        $roleUser = Yii::$app->authManager->createRole('user');
        $roleUser->description = 'Пользователь';
        Yii::$app->authManager->add($roleUser);

        $roleModer = Yii::$app->authManager->createRole('moderator');
        $roleModer->description = 'Модератор';
        Yii::$app->authManager->add($roleModer);


        $roleAdmin = Yii::$app->authManager->createRole('admin');
        $roleAdmin->description = 'Админ';
        Yii::$app->authManager->add($roleAdmin);


        foreach($this->permissions as $name=>$desc){
            $perm = Yii::$app->authManager->createPermission($name);
            $perm->description = $desc;
            Yii::$app->authManager->add($perm);
            Yii::$app->authManager->addChild($roleAdmin, $perm);

            Yii::$app->authManager->addChild($roleModer, $perm);

        }
        foreach($this->permissionsAdmin as $name=>$desc){
            $perm = Yii::$app->authManager->createPermission($name);
            $perm->description = $desc;
            Yii::$app->authManager->add($perm);
            Yii::$app->authManager->addChild($roleAdmin, $perm);
        }

    }

    public function safeDown()
    {
        foreach($this->permissions as $name=>$desc){
            $perm = Yii::$app->authManager->getPermission($name);
            if ($perm)
                Yii::$app->authManager->remove($perm);
        }

        foreach($this->permissionsAdmin as $name=>$desc){
            $perm = Yii::$app->authManager->getPermission($name);
            if ($perm)
                Yii::$app->authManager->remove($perm);
        }

        echo "Права удалены";

        $role = Yii::$app->authManager->getRole('admin');
        if ($role) {
            Yii::$app->authManager->removeChildren($role);
            Yii::$app->authManager->remove($role);
        }

        $role = Yii::$app->authManager->getRole('moderator');
        if ($role) {
            Yii::$app->authManager->removeChildren($role);
            Yii::$app->authManager->remove($role);
        }
        $role = Yii::$app->authManager->getRole('user');
        if ($role) {
            Yii::$app->authManager->removeChildren($role);
            Yii::$app->authManager->remove($role);
        }

    }
}

