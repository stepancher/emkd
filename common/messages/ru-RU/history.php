<?php
return [
    'Create_purchase' => 'Create purchase',
    'Create_order' => 'Create order',
    'PURCHASE_CREATE'=>'{user} создал закупку',
    'PURCHASE_CHANGE_STATUS'=>'{user} изменил статус закупки с "{old_status}" на "{new_status}"',
    'ORDER_CHANGE_STATUS'=>'{user} изменил статус заказа с "{old_status}" на "{new_status}"',
    'ORDER_CREATE'=>'{user} оформил заказ',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
];