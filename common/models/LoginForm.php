<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;
    private $_user = false;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules= [
            // username and password are both required
            [['email', 'password'], 'required', 'message'=>\Yii::t('auth', 'can\'t be empty')],
            ['email', 'email', 'message'=>\Yii::t('auth', 'incorrect email')],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
        /**
         * Проверяем количество попыток ввода логин/пароля
         * после трех проверяем на капчу
         */
        $bad_try_input_ip = \Yii::$app->cache->get('bad_try_input_ip_'.\Yii::$app->request->getUserIP());
        $bad_try_input_email = \Yii::$app->cache->get('bad_try_input_email_'.$this->email);

        return $rules;
    }

    /**
     * Валидация логина/пароля
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {

        if (!$this->hasErrors()) {
            $user = $this->getUser();

            $keyIp='bad_try_input_ip_'.\Yii::$app->request->getUserIP();
            $keyEmail='bad_try_input_email_'.$this->email;
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, \Yii::t('auth','Incorrect username or password.'));

                /*счетчик неудачных попыток входа по Ip*/
                $bad_try_input_ip = \Yii::$app->cache->get($keyIp);
                if($bad_try_input_ip){
                    \Yii::$app->cache->set($keyIp,++$bad_try_input_ip);
                }else{
                    \Yii::$app->cache->set($keyIp,1);
                }
                $bad_try_input_email = \Yii::$app->cache->get($keyEmail);
                if($bad_try_input_email){
                    \Yii::$app->cache->set($keyEmail,++$bad_try_input_email);
                }else{
                    \Yii::$app->cache->set($keyEmail,1);
                }

            }else{
                \Yii::$app->cache->delete($keyEmail);
                \Yii::$app->cache->delete($keyIp);
            }
        }

    }

    /**
     * Если все хорошо, Логируем юзера
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
    /**
     * проверяем существует ли юзер с таким неподтвержденным email
     *
     * @return true/false
     */
    public function checkNoActivateUser()
    {
        return User::findNoActivateByEmail($this->email);
    }
    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => \Yii::t('auth','email'),
            'password' => \Yii::t('auth','password'),
            'rememberMe' => \Yii::t('auth','запомнить меня'),
        ];
    }
}
