<?php

namespace common\models;
use vova07\fileapi\behaviors\UploadBehavior;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $id_user
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $date_birthday
 * @property string $sex
 * @property string $avatar
 * @property string $skype
 * @property string $icq
 * @property string $notes
 *
 * @property User $idUser
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'avatar' => [
                        'path' => \Yii::$app->params['userAvatarDir'],
                        'tempPath' => \Yii::$app->params['uploadTempDir'],
                        'url' => \Yii::$app->params['userAvatarUrl'],
                    ]
                ]
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'middlename', 'lastname', 'notes', 'sex', 'skype', 'icq'], 'filter', 'filter' => 'trim'],
            [['firstname', 'middlename', 'lastname', 'notes', 'sex', 'skype', 'icq'], 'filter', 'filter' => 'strip_tags'],
            [['id_user'], 'integer'],
            [['date_birthday'], 'safe'],
            [['notes'], 'string'],
            [['firstname', 'middlename', 'lastname'], 'string', 'max' => 100, 'tooLong' => \Yii::t('auth', 'maximum character',['n'=>100])],
            [['sex'], 'string', 'max' => 1],
            [['avatar', 'skype', 'icq'], 'string', 'max' => 255, 'tooLong' => \Yii::t('auth', 'maximum character',['n'=>255])],
        ];
    }

    /**
     * Ссылка на публичную страницу пользователя
     * @return string
     */
    public function getUrl()
    {
        return Url ::to(['user/profile','idUser'=>$this->id_user]);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => Yii::t('auth', 'Id User'),
            'firstname' => Yii::t('auth', 'First name'),
            'middlename' => Yii::t('auth', 'Middle name'),
            'lastname' => Yii::t('auth', 'Last name'),
            'date_birthday' => Yii::t('auth', 'Date Birthday'),
            'sex' => Yii::t('auth', 'Sex'),
            'avatar' => Yii::t('auth', 'Avatar'),
            'skype' => Yii::t('auth', 'Skype'),
            'icq' => Yii::t('auth', 'Icq'),
            'notes' => Yii::t('auth', 'Notes'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * Возвращает строку имя_фамилия
     * @return \yii\db\ActiveQuery
     */
    public function getFio()
    {
        return trim($this->firstname.' '.$this->lastname);
    }

    /**
     * Возвращает аватар пользователя
     * @return string
     */
	public function getProfileAvatar()
	{
		if ($this->avatar != '') {
			return Yii::$app->params['userAvatarUrl']. $this->avatar;
		}

		if ($this->sex == 1) {
			return '/img/no-avatar-male.gif';
		}

		return '/img/no-avatar-female.gif';
	}

    /**
     * Загружает аватарку пользователя по URL
     * Метод используется при регистрации пользователя через соц. сеть
     * @param $url
     * @return bool|int
     */
    public function setProfileAvatar($url)
    {
        $this->avatar = md5($this->id_user . '-' . time()) . '.jpg';
        $isExists = is_array(@get_headers($url)) && preg_match("|200|", @get_headers($url)[0]);

        if ($isExists && file_put_contents(Yii::getAlias('@root' . Yii::$app->params['userAvatarUrl'] . $this->avatar), file_get_contents($url))) {
            return $this->updateAttributes([
                'avatar' => $this->avatar
            ]);
        }
        return false;
    }
}
