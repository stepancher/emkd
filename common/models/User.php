<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;


/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $email_confirm_token
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
	const SEX_MALE = 1;     // М
	const SEX_FEMALE = 0;   // Ж

    const STATUS_DELETED = 0;
    const STATUS_NOACTIVE = 5;
    const STATUS_ACTIVE = 10;

    public function getActiveStatus()
    {
        return self::STATUS_ACTIVE;
    }

    public function isActive()
    {
        return $this->status===self::STATUS_ACTIVE;
    }

    public function setActiveStatus()
    {
        $this->status = self::STATUS_ACTIVE;
    }

    public function getStatusLabel()
    {
        $label=[0=>'Deleted',5=>'No Active',10=>'Active'];
        return $label[$this->status];
    }

    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_NOACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_NOACTIVE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::find()
            ->where(['email' => $email, 'status' => self::STATUS_ACTIVE])
            ->one();
    }

    public static function findNoActivateByEmail($email)
    {
        return static::find()
            ->where(['email' => $email,'status'=>self::STATUS_NOACTIVE])
            ->exists();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by email confirm token
     *
     * @param string $token email confirm token
     * @return static|null
     */
    public static function findByEmailConfirmToken($token)
    {
        return static::findOne([
            'email_confirm_token' => $token
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * Finds out if email confirm token is valid
     *
     * @param string $token email confirm token
     * @return boolean
     */
    public static function isEmailConfirmTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordEmailConfirmExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Set status active
     *
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new email confirm token
     */
    public function generateEmailConfirmToken()
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Removes email confirm token
     */
    public function removeEmailConfirmToken()
    {
        $this->email_confirm_token = null;
    }

    public function confirmEmail()
    {
        $this->setStatus(self::STATUS_ACTIVE);
        $this->removeEmailConfirmToken();
        return $this->save();
    }

	/* * */

	public function getProfile()
	{
		return $this->hasOne('common\models\UserProfile', ['id_user' => 'id']);
	}

	static public function getAllToList()
	{
		$album = new static();
		$list = $album
			->find()
			->orderBy('username')
			->asArray()
			->all();

		$keys = array_map(
			function ($item) {
				return $item['id'];
			}, $list);

		$values = array_map(
			function ($item) {
				return $item['username'] . '(' . $item['email'] . ')';
			}, $list);

		return array_combine($keys, $values);
	}

    /**
     * Связь пользователя с альбомами
     * @return \yii\db\ActiveQuery
     */
    public function getAlbums()
    {
        return $this
            ->hasMany('common\models\Album', ['id_user' => 'id']);
    }

    /**
     * Возвращает все альбомы пользователя
     * @return array
     */
    public function getAlbumsToList()
    {
        return ArrayHelper::map(
            $this->getAlbums()->asArray()->all(),
            'id',
            'title'
        );
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => \Yii::t('auth','username'),
            'email' =>\Yii::t('auth','email'),
            'status' => \Yii::t('auth','status'),
            'created_at' => \Yii::t('auth','created_at'),
            'updated_at' => \Yii::t('auth','updated_at'),
            'password' => \Yii::t('auth','password'),
        ];
    }
}