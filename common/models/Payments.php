<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "payments".
 *
 * @property integer $id
 * @property string $date_add
 * @property string $date_pay
 * @property string $date_enrolled
 * @property string $type
 * @property string $num_bank
 * @property string $summ
 * @property string $payment_info1
 * @property string $payment_info2
 * @property string $payment_info3
 * @property string $payment_info4
 * @property integer $status
 * @property string $acc_for
 * @property string $hash
 * @property integer $created_at
 * @property integer $updated_at
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments';
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],

        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_add', 'date_pay', 'date_enrolled'], 'safe'],
            [['type', 'num_bank', 'payment_info1', 'payment_info2', 'payment_info3', 'payment_info4', 'hash'], 'string'],
            [['summ'], 'number'],
            [['status','id_user'], 'integer'],
            [['acc_for', 'hash'], 'required'],
            [['acc_for'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('payments', 'ID'),
            'id_user' => Yii::t('payments', 'User'),

            'date_add' => Yii::t('payments', 'Date Add'),
            'date_pay' => Yii::t('payments', 'Date Pay'),
            'date_enrolled' => Yii::t('payments', 'Date Enrolled'),
            'type' => Yii::t('payments', 'Type'),
            'num_bank' => Yii::t('payments', 'Num Bank'),
            'summ' => Yii::t('payments', 'Summ'),
            'payment_info1' => Yii::t('payments', 'Payment Info1'),
            'payment_info2' => Yii::t('payments', 'Payment Info2'),
            'payment_info3' => Yii::t('payments', 'Payment Info3'),
            'payment_info4' => Yii::t('payments', 'Payment Info4'),
            'status' => Yii::t('payments', 'Status'),
            'acc_for' => Yii::t('payments', 'Acc For'),
            'hash' => Yii::t('payments', 'Hash'),
            'created_at' => Yii::t('payments', 'Created At'),
            'updated_at' => Yii::t('payments', 'Updated At'),
        ];
    }
}
