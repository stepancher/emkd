<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Payments;

/**
 * PaymentsSearch represents the model behind the search form about `common\models\Payments`.
 */
class PaymentsSearch extends Payments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['date_add', 'date_pay', 'date_enrolled', 'type', 'num_bank', 'payment_info1', 'payment_info2', 'payment_info3', 'payment_info4', 'acc_for', 'hash', 'created_at', 'updated_at','id_user'], 'safe'],
            [['summ'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payments::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_add' => $this->date_add,
            'date_pay' => $this->date_pay,
            'date_enrolled' => $this->date_enrolled,
            'summ' => $this->summ,
            'status' => $this->status,
            'id_user' => $this->id_user,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'num_bank', $this->num_bank])
            ->andFilterWhere(['like', 'payment_info1', $this->payment_info1])
            ->andFilterWhere(['like', 'payment_info2', $this->payment_info2])
            ->andFilterWhere(['like', 'payment_info3', $this->payment_info3])
            ->andFilterWhere(['like', 'payment_info4', $this->payment_info4])
            ->andFilterWhere(['like', 'acc_for', $this->acc_for])
            ->andFilterWhere(['like', 'hash', $this->hash]);

        return $dataProvider;
    }
}
