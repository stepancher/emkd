<?php
return [
    'adminEmail' => 'admin@example.ru',
    'supportEmail' => 'support@example.ru',
    'user.passwordResetTokenExpire' => 7*24*60*60,   // Время жизни токена - 7 дней
    'user.passwordEmailConfirmExpire' => 7*24*60*60, // Время жизни токена - 7 дней

    'uploadTempDir'=>'@root/upload/temp',
    'uploadTempUrl'=>'/upload/temp/',

    'userAvatarDir'=>'@root/upload/user/',
    'userAvatarUrl'=>'/upload/user/',

];
