<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'db' => require(__DIR__ . '/db.php'),
        'mailer' => require(__DIR__ . '/mail.php'),
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'loginUrl' => ['login'],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['Guest'],
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.Y',
            'timeFormat' => 'hh:mm:ss',
            'datetimeFormat' => 'd.m.Y H:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'nullDisplay' => true
        ],
        'i18n' => require(__DIR__ . '/i18n.php'),
        'cache' => [
            'useMemcached'=>true,
            'keyPrefix'=>'gw_',
            'class' => 'yii\caching\MemCache',
            'servers' => [
                [
                    'host' => '127.0.0.1',
                    'port' => 11211,
                    'weight' => 60,
                ],
            ],
        ],

    ],
];
