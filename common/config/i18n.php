<?php

return [

    'translations' => [
        'yii*' => [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@common/messages',
        ],
        'app*' => [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@common/messages',
        ],
        'auth*' => [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@common/messages',
        ],
        'payments' => [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@common/messages',
        ],
    /*
      content локализацию ищи в модуле
     */
    ],
];
