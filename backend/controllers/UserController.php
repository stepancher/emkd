<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use backend\models\UserForm;
use backend\models\RoleForm;
use common\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;
use yii\helpers\Url;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{

    /**
     * Поведение
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['r_user'], //все действия по работе с юзерами только для админа
                    ],
                    [
                        'actions' => ['create','update','delete'],
                        'allow' => true,
                        'roles' => ['w_user'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $userForm = new UserForm();
        if ($userForm->load(Yii::$app->request->post())) {
            if ($userForm->signup()) {
                Yii::$app->session->setFlash('success', \Yii::t('auth', 'User successful create'));
                return $this->redirect('index');
            }
        }

        return $this->render('create', [
            'userForm' => $userForm
        ]);
    }

    /**
     * Updates an existing UserForm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /**  @var $user User*/
        $user = User::findOne($id);
        $userForm = new UserForm(false);
        $userForm->attributes = $user->attributes; //суем данные из User в форму



        if ($userForm->load(Yii::$app->request->post())) {
            if ($userForm->update()) {
                return $this->redirect(['index']);
            }
        }
        $userForm->roles = array_keys(Yii::$app->authManager->getRolesByUser($user->getId()));
        $userForm->permissions = array_keys(Yii::$app->authManager->getAssignments($user->getId()));
        return $this->render('create', [
            'userForm' => $userForm,
        ]);
    }


    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModelUser($id)->delete();
        Yii::$app->session->setFlash('success', \Yii::t('auth', 'User successful deleted'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelUser($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ooops, unexpected error');
        }
    }
}