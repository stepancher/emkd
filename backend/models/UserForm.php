<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Class UserForm Форма сохдания и редактирования юзера в админке
 * @package backend\models

 * @property string $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $roles
 * @property string $permissions
 * @property string $isNewRecord
 *
 */
class UserForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $roles;
    public $permissions;
    public $id;
    public $isNewRecord=true;

    public function __construct($isNewRecord=true){
        $this->isNewRecord = $isNewRecord;

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['username', 'filter', 'filter' => 'trim'],
            [['username', 'email'],'required', 'message'=>\Yii::t('auth', 'can\'t be empty')],
            ['username', 'string', 'min' => 2, 'max' => 100, 'tooShort' => \Yii::t('auth', 'minimum character',['n'=>2]), 'tooLong' => \Yii::t('auth', 'maximum character',['n'=>255])],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],

            ['password', 'string', 'min' => 6, 'tooShort' => \Yii::t('auth', 'minimum character',['n'=>6])],
            [['roles','id','permissions'], 'safe'],
            [['id'], 'integer']
        ];
        if($this->isNewRecord){
            $rules[] = ['password', 'required', 'message'=>\Yii::t('auth', 'can\'t be empty')];

            $rules[] = ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => \Yii::t('auth', 'This username has already been taken.')];
            $rules[] = ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => \Yii::t('auth', 'This email address has already been taken.')];
        }
        return $rules;

    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->setActiveStatus();
            if ($user->save()) {
                Yii::$app->authManager->revokeAll($user->id); //Удаляем все роли/пермишены юзера

                if(count($this->roles)){
                    foreach($this->roles as $role){
                        $newRole = Yii::$app->authManager->getRole($role); //создаем объект роли по имени
                        Yii::$app->authManager->assign($newRole, $user->id);
                    }
                }
                if(count($this->permissions)){
                    foreach($this->permissions as $perm){
                        $newPerm = Yii::$app->authManager->getPermission($perm); //создаем объект роли по имени
                        Yii::$app->authManager->assign($newPerm, $user->id);
                    }
                }
                return $user;
            }
        }
        return null;
    }

    /**
     * Изменение  пользователя
     *
     * @return true/false saved 2 model User and UserProfile
     */
    public function update()
    {
        if ($this->validate()) {
            /**  @var $user User*/
            $user = User::findOne($this->id);

            $user->username = $this->username;
            $user->email = $this->email;
            if($this->password)
                $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->setActiveStatus();
            if ($user->save()) {
                /**
                 * Юзера сохранили сохраняем права
                 */
                Yii::$app->authManager->revokeAll($user->id); //Удаляем все роли/пермишены юзера

                if(count($this->roles)){
                    foreach($this->roles as $role){
                        $newRole = Yii::$app->authManager->getRole($role); //создаем объект роли по имени
                        Yii::$app->authManager->assign($newRole, $user->id);
                    }
                }
                if(count($this->permissions)){
                    foreach($this->permissions as $perm){
                        $newPerm = Yii::$app->authManager->getPermission($perm); //создаем объект роли по имени
                        Yii::$app->authManager->assign($newPerm, $user->id);
                    }
                }
                return true;
            }
        }

        return false;
    }

    /**
     * Массив лэйблов локализованный
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username' => \Yii::t('auth','username'),
            'email' =>\Yii::t('auth','email'),
            'password' => \Yii::t('auth','password'),
        ];
    }
}
