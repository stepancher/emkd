<?php
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Правила доступа';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Добавить значение</h3>
        </div>
        <div class="box-body">
            <?php
            if (!empty($error)) {
                ?>
                <div class="error-summary">
                    <?php
                    echo implode('<br>', $error);
                    ?>
                </div>
            <?php
            }
            ?>
            <?php $form = ActiveForm::begin(['id' => 'form-permit-add', 'options' => ['class' => 'form-horizontal']]); ?>
            <div class="input-group input-group-sm">

                    <?= Html::textInput('name','',['placeholder'=>'правило','class' => 'form-control ref-value',]); ?>


                    <?= Html::textInput('description','',['placeholder'=>'Описание','class' => 'form-control ref-value',]); ?>

                <span class="input-group-btn">
		            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat', 'name' => 'save-button']) ?>
	            </span>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>



    <?php
    $dataProvider = new ArrayDataProvider([
          'allModels' => Yii::$app->authManager->getPermissions(),
          'sort' => [
              'attributes' => ['name', 'description'],
          ],
          'pagination' => [
              'pageSize' => 10,
          ],
     ]);
    ?>
    <div class="box">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "<div class='box-body'>{items}</div><div class='box-footer'><div class='row'><div class='col-sm-6'>{summary}</div><div class='col-sm-6'>{pager}</div></div></div>",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class'     => DataColumn::className(),
                    'attribute' => 'name',
                    'label'     => 'Правило'
                ],
                [
                    'class'     => DataColumn::className(),
                    'attribute' => 'description',
                    'label'     => 'Описание'
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttons' =>
                        [
                            'update' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::toRoute(['update-permission', 'name' => $model->name]), [
                                                'title' => Yii::t('yii', 'Update'),
                                                'data-pjax' => '0',
                                            ]); },
                            'delete' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::toRoute(['delete-permission','name' => $model->name]), [
                                                'title' => Yii::t('yii', 'Delete'),
                                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                                'data-method' => 'post',
                                                'data-pjax' => '0',
                                            ]);
                                }
                        ]
                ],
                ]
            ]);
        ?>
    </div>
</div>