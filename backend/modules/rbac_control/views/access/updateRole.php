<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use nex\chosen\Chosen;

/* @var $this yii\web\View */
/* @var $model common\models\Links */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Редактирование роли: ' . ' ' . $role->name;
$this->params['breadcrumbs'][] = ['label' => 'Управление ролями', 'url' => ['role']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>


<div class="row">
    <div class="col-sm-6">
        <div class="box">
            <div class="box-body">
                <?php
                if (!empty($error)) {
                    ?>
                    <div class="error-summary">
                        <?php
                        echo implode('<br>', $error);
                        ?>
                    </div>
                <?php
                }
                ?>

                <?php $form = ActiveForm::begin(); ?>

                <div class="form-group">
                    <?= Html::label('Название роли',['class' => 'form-control ref-value']); ?><br/>
                    <?= Html::textInput('name', $role->name,['placeholder'=>'Название роли','class' => 'form-control ref-value']); ?>
                </div>

                <div class="form-group">
                    <?= Html::label('Текстовое описание'); ?><br/>
                    <?= Html::textInput('description', $role->description,['placeholder'=>'Название роли','class' => 'form-control ref-value']); ?>
                </div>

                <div class="form-group">
                    <?= Html::label('Выберите права'); ?><br/>
                    <?= Chosen::widget([
                        'placeholder' => 'Выберите права',
                        'name' => 'permissions',
                        'value' => $role_permit,
                        'items' => $permissions,
                        'multiple' => true,
                    ]); ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
