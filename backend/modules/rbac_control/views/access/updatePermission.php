<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Links */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Редактирование правила: ' . ' ' . $permit->description;
$this->params['breadcrumbs'][] = ['label' => 'Правила доступа', 'url' => ['permission']];
$this->params['breadcrumbs'][] = 'Редактирование правила';
?>

<div class="row">
    <div class="col-sm-6">
        <div class="box">
            <div class="box-body">
                <?php
                if (!empty($error)) {
                    ?>
                    <div class="error-summary">
                        <?php
                        echo implode('<br>', $error);
                        ?>
                    </div>
                <?php
                }
                ?>

                <?php $form = ActiveForm::begin(); ?>

                <div class="form-group">

                    <?= Html::label('Право',['class' => 'form-control ref-value']); ?><br/>
                    <?= Html::textInput('name', $permit->name,['placeholder'=>'Название права','class' => 'form-control ref-value']); ?>
                </div>


                <div class="form-group">
                    <?= Html::label('Описание',['class' => 'form-control ref-value']); ?><br/>
                    <?= Html::textInput('description', $permit->description,['placeholder'=>'Описание права','class' => 'form-control ref-value']); ?>

                </div>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>