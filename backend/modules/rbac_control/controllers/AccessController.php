<?php
/**
 * AccessController for Yii2
 *
 * @author Elle <elleuz@gmail.com>
 * @version 0.1
 * @package AccessController for Yii2
 *
 */
namespace backend\modules\rbac_control\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\rbac\Role;
use yii\rbac\Permission;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\validators\RegularExpressionValidator;

class AccessController extends Controller
{
    protected $error;
    protected $pattern4Role = '/^[a-zA-Z0-9-_]+$/';
    protected $pattern4Permission = '/^[a-zA-Z0-9_\/]+$/';


    /**
     * Поведение
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['role','index','permission'],
                        'allow' => true,
                        'roles' => ['r_role'], //все действия по работе с юзерами только для админа
                    ],
                    [
                        'actions' => ['update-role','delete-role','update-permission','delete-permission'],
                        'allow' => true,
                        'roles' => ['w_role'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображение всех ролей
     * @return string
     */
    public function actionRole()
    {
        if (Yii::$app->request->post('name')
            && $this->validate(Yii::$app->request->post('name'), $this->pattern4Role)
            && $this->isUnique(Yii::$app->request->post('name'), 'role')
        ) {
            $role = Yii::$app->authManager->createRole(Yii::$app->request->post('name'));
            $role->description = Yii::$app->request->post('description');
            Yii::$app->authManager->add($role);
            $this->setPermissions(Yii::$app->request->post('permissions', []), $role);
            Yii::$app->session->setFlash('success', \Yii::t('rbac_control', 'Role successful create'));
        }
        $permissions = ArrayHelper::map(Yii::$app->authManager->getPermissions(), 'name', 'description');
        return $this->render(
            'role',
            [
                'permissions' => $permissions,
                'error' => $this->error
            ]
        );
    }
    /**
     * Изменение роли
     * @param $name
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionUpdateRole($name)
    {
        $role = Yii::$app->authManager->getRole($name);

        if ($role instanceof Role) {
            if (Yii::$app->request->post('name')
                && $this->validate(Yii::$app->request->post('name'), $this->pattern4Role)
            ) {
                $role = $this->setAttribute($role, Yii::$app->request->post());
                Yii::$app->authManager->update($name, $role);
                Yii::$app->authManager->removeChildren($role);
                $this->setPermissions(Yii::$app->request->post('permissions', []), $role);
                return $this->redirect(Url::toRoute(['update-role', 'name' => $role->name]));
            }

            $permissions = ArrayHelper::map(Yii::$app->authManager->getPermissions(), 'name', 'description');
            $role_permit = array_keys(Yii::$app->authManager->getPermissionsByRole($name));

            return $this->render(
                'updateRole',
                [
                    'role' => $role,
                    'permissions' => $permissions,
                    'role_permit' => $role_permit,
                    'error' => $this->error
                ]
            );
        } else {
            throw new BadRequestHttpException(Yii::t('rbac_control', 'Page not found'));
        }
    }

    /**
     * Удаление роли
     * @param $name
     * @return \yii\web\Response
     */
    public function actionDeleteRole($name)
    {
        $role = Yii::$app->authManager->getRole($name);
        if ($role) {
            Yii::$app->authManager->removeChildren($role);
            Yii::$app->authManager->remove($role);
        }
        return $this->redirect(Url::toRoute(['role']));
    }

    /**
     * Отображение списка разрешений
     * @return string
     */
    public function actionPermission()
    {
        if (Yii::$app->request->post('name')
            && $this->validate(Yii::$app->request->post('name'), $this->pattern4Permission)
            && $this->isUnique(Yii::$app->request->post('name'), 'permission')
        ) {
            $permit = Yii::$app->authManager->createPermission(Yii::$app->request->post('name'));
            $permit->description = Yii::$app->request->post('description', '');
            Yii::$app->authManager->add($permit);
            Yii::$app->session->setFlash('success', \Yii::t('rbac_control', 'Permission successful create'));

        }
        return $this->render('permission', ['error' => $this->error]);
    }


    /**
     * @param $name наименование разрешения
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionUpdatePermission($name)
    {
        $permit = Yii::$app->authManager->getPermission($name);
        if ($permit instanceof Permission) {
            if (Yii::$app->request->post('name')
                && $this->validate(Yii::$app->request->post('name'), $this->pattern4Permission)
            ){
                $permit = $this->setAttribute($permit, Yii::$app->request->post());
                Yii::$app->authManager->update($name, $permit);
                return $this->redirect(Url::toRoute(['update-permission', 'name' => $permit->name]));
            }

            return $this->render('updatePermission', ['permit' => $permit, 'error' => $this->error]);
        } else throw new BadRequestHttpException(Yii::t('rbac_control', 'Page not found'));
    }

    /**
     * Удаление разрешения
     * @param $name
     * @return \yii\web\Response
     */
    public function actionDeletePermission($name)
    {
        $permit = Yii::$app->authManager->getPermission($name);
        if ($permit)
            Yii::$app->authManager->remove($permit);
        return $this->redirect(Url::toRoute(['permission']));
    }

    /**
     * Установка аттрибутов Роли/Разрешения
     * @param $object
     * @param $data
     * @return mixed
     */
    protected function setAttribute($object, $data)
    {
        $object->name = $data['name'];
        $object->description = $data['description'];
        return $object;
    }

    /**
     * Установка привязки списка разрешений к роли
     * @param $permissions
     * @param $role
     */
    protected function setPermissions($permissions, $role)
    {
        foreach ($permissions as $permit) {
            $new_permit = Yii::$app->authManager->getPermission($permit);
            Yii::$app->authManager->addChild($role, $new_permit);
        }
    }

    /**
     * Валидация полей
     * @param $field
     * @param $regex
     * @return bool
     */
    protected function validate($field, $regex)
    {
        $validator = new RegularExpressionValidator(['pattern' => $regex]);
        if ($validator->validate($field, $error))
            return true;
        else {
            $this->error[] = Yii::t('rbac_control', 'The value "{field}" contains not allowed characters', ['field' => $field]);
            return false;
        };
    }


    /**
     * Проверка уникалиности Роли/Разрешения
     * @param $name имя проверяемого
     * @param $type тип - > ['role','permission']
     * @return bool
     */
    protected function isUnique($name, $type)
    {
        if ($type == 'role') {
            $role = Yii::$app->authManager->getRole($name);
            if ($role instanceof Role) {
                $this->error[] = Yii::t('rbac_control', 'This role already exist: ') . $name;
                return false;
            } else return true;
        } elseif ($type == 'permission') {
            $permission = Yii::$app->authManager->getPermission($name);
            if ($permission instanceof Permission) {
                $this->error[] = Yii::t('rbac_control', 'This permission already exist: ') . $name;
                return false;
            } else return true;
        }
    }
}