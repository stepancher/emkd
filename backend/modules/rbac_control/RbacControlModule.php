<?php
/**
 * Yii2DbRbac for Yii2
 *
 * @author Elle <elleuz@gmail.com>
 * @version 0.1
 * @package Yii2DbRbac for Yii2
 *
 */
namespace backend\modules\rbac_control;

use Yii;

class RbacControlModule extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\rbac_control\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['rbac_control'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@backend/modules/rbac_control/messages',
        ];
    }


}
