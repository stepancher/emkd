<?php
    use yii\bootstrap\Nav;
    use yii\helpers\Url;
    use yii\helpers\Html;
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <?php if (Yii::$app->user->can('r_user')): ?>
            <li class="treeview <?php if(in_array(Yii::$app->requestedRoute, ['user','user/create','user/update','permit/access/role','permit/access/update-role','permit/access/permission','permit/access/update-permission'])) : ?>active<?php endif; ?>">
                <a href="<?= $directoryAsset ?>/#">
                    <i class="glyphicon glyphicon-user"></i>
                    Пользователи и права
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if(in_array(Yii::$app->requestedRoute, ['user','user/create','user/update'])) : ?>active<?php endif; ?>">
                        <a href="<?= Url::toRoute(['/user']) ?>">
                            <i class="fa fa-angle-double-right"></i> <span>Пользователи</span>
                        </a>
                    </li>
                    <li class="<?php if(in_array(Yii::$app->requestedRoute, ['permit/access/role','permit/access/update-role'])) : ?>active<?php endif; ?>">
                        <a href="<?= Url::toRoute(['/permit/access/role']) ?>">
                            <i class="fa fa-angle-double-right"></i>
                            Роли
                        </a>
                    </li>
                    <li class="<?php if(in_array(Yii::$app->requestedRoute, ['permit/access/permission','permit/access/update-permission'])) : ?>active<?php endif; ?>">
                        <a href="<?= Url::toRoute(['/permit/access/permission']) ?>">
                            <i class="fa fa-angle-double-right"></i>
                            Права
                        </a>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if (Yii::$app->user->can('r_payments')): ?>
                <li class=" <?php if (in_array(Yii::$app->requestedRoute, ['payments','payments/index','payments/create','payments/update'])) : ?>active<?php endif; ?>">
                    <a href="<?= Url::toRoute(['/payments']) ?>">
                        <i class="fa fa-file"></i> <span>Платежи</span>
                    </a>
                </li>
            <?php endif; ?>
        </ul>

        <hr style="margin:0;"/>

        <?=
        Nav::widget(
                [
                    'encodeLabels' => false,
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => '<span class="fa fa-file-code-o"></span> Gii', 'url' => ['/gii']],
                        ['label' => '<span class="fa fa-dashboard"></span> Debug', 'url' => ['/debug']],
                    ],
                ]
        );
        ?>
    </section>

</aside>
