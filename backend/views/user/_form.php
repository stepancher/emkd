<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use nex\chosen\Chosen;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $userForm backend\models\UserForm*/

?>


<div class="box">
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>


        <?= $form->field($userForm, 'username')->textInput(['maxlength' => 100]) ?>
        <?= $form->field($userForm, 'email') ?>
        <?= $form->field($userForm, 'password',[
            'template' => $userForm->isNewRecord?'<div>{label}{input}{hint}{error}</div>':'<div>{label}(введите для смены){input}{hint}{error}</div>',
        ]) ?>
        <?= Chosen::widget([
            'placeholder' => 'Выберите роли',
            'model' => $userForm,
            'attribute' => 'roles',
            'items' => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description'),
            'multiple' => true,
        ]); ?>
        <?= Chosen::widget([
            'placeholder' => 'Выберите права',
            'model' => $userForm,
            'attribute' => 'permissions',
            'items' => ArrayHelper::map(Yii::$app->authManager->getPermissions(), 'name', 'description'),
            'multiple' => true,
        ]); ?>
        <div class="help-block"></div>
        <?= Html::error($userForm, 'roles', [
            'class' => 'help-block'
        ])
        ?>
        <?= Html::activeInput('hidden', $userForm, 'id') ?>



        <div class="form-group">
            <?= Html::submitButton($userForm->isNewRecord ? Yii::t('auth', 'Create') : Yii::t('auth', 'Update'), ['class' => $userForm->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
</div>
</div>
