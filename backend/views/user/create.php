<?php


use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $userForm backend\models\UserForm */


$this->title = \Yii::t('auth', 'Create user');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('auth', 'users'), 'url' => ['/user']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="user-create">

        <div class="col-sm-5">

            <p><?= \Yii::t('auth', 'Please fill out the following fields to signup:') ?></p>

            <?php
            echo $this->render('_form', ['userForm' => $userForm])
            ?>


        </div>
    </div>
</div>






