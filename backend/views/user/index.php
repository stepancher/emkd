<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/** @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="page-header">
        <div class="panel-heading-controls">
            <?= Html::a('<i class="btn-label icon fa fa-plus"></i> '.\Yii::t('auth','Create user'), ['create'], ['class' => 'btn btn-labeled btn-primary no-margin-t']) ?>
        </div>
    </div>



    <div class="box">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "<div class='box-body'>{items}</div><div class='box-footer'><div class='row'><div class='col-sm-6'>{summary}</div><div class='col-sm-6'>{pager}</div></div></div>",
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'id',
                'format'=>'raw',
                'value'=>function($data){
                    return Html::a($data->id,\yii\helpers\Url::toRoute(['/user/update','id'=>$data->id]));
                },
            ],
            [
                'attribute'=>'username',
                'format'=>'raw',
                'value'=>function($data){
                    return Html::a($data->username,\yii\helpers\Url::toRoute(['/user/update','id'=>$data->id]));
                },
            ],
            'email:email',
            [
                'attribute'=>'status',
                'format'=>'raw',
                'value'=>function($data){
                    return $data->getStatusLabel();
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ],
    ]); ?>

    </div>

