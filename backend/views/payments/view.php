<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Payments */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('payments', 'Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('payments', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('payments', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('payments', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date_add',
            'date_pay',
            'date_enrolled',
            'type:ntext',
            'num_bank:ntext',
            'summ',
            'payment_info1:ntext',
            'payment_info2:ntext',
            'payment_info3:ntext',
            'payment_info4:ntext',
            'status',
            'acc_for',
            'hash:ntext',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
