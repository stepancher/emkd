<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Payments */

$this->title = Yii::t('payments', 'Create Payments');
$this->params['breadcrumbs'][] = ['label' => Yii::t('payments', 'Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
