<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Payments */

$this->title = Yii::t('payments', 'Update payments: ', [
    'modelClass' => 'Payments',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('payments', 'Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('payments', 'Update');
?>
<div class="payments-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
