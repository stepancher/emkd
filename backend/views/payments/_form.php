<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Payments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_user')->widget(
        \nex\chosen\Chosen::className(),
        [
            'placeholder' => 'Владелец',
            'items' => \common\models\User::getAllToList(),
            'multiple' => false
        ]
    ); ?>

    <?= $form->field($model, 'date_add')->widget(\kartik\datetime\DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Введите время события ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd hh:ii:ss',
            'autoclose' => true,
            'todayBtn' => true,
            'showMeridian' => true
        ]
    ]); ?>
    <?= $form->field($model, 'date_pay')->widget(\kartik\datetime\DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Введите время события ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd hh:ii:ss',
            'autoclose' => true,
            'todayBtn' => true,
            'showMeridian' => true
        ]
    ]); ?>
    <?= $form->field($model, 'date_enrolled')->widget(\kartik\datetime\DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Введите время события ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd hh:ii:ss',
            'autoclose' => true,
            'todayBtn' => true,
            'showMeridian' => true
        ]
    ]); ?>


    <?= $form->field($model, 'type')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'num_bank')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'summ')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_info1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'payment_info2')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'payment_info3')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'payment_info4')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'acc_for')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hash')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('payments', 'Create') : Yii::t('payments', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
