<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
if (in_array(Yii::$app->controller->action->id,['login','request-password-reset','reset-password','email-confirm','signup'])) {
	echo $this->render(
		'wrapper-black',
		['content' => $content]
	);
} else {
	dmstr\web\AdminLteAsset::register($this);
	$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@bower') . '/admin-lte';
	?>


	<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
                <script src="" type="text/javascript"></script>
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>
	</head>
	<body class="skin-black">

	<?php $this->beginBody() ?>

	<?= $this->render(
		'header.php',
		['directoryAsset' => $directoryAsset]
	) ?>

	<div class="wrapper row-offcanvas row-offcanvas-left">

		<?= $this->render(
			'left.php',
			['directoryAsset' => $directoryAsset]
		)
		?>

		<?= $this->render(
			'content.php',
			['content' => $content, 'directoryAsset' => $directoryAsset]
		) ?>

	</div>

	<?php $this->endBody() ?>
	</body>
	</html>
	<?php $this->endPage() ?>
<?php } ?>
