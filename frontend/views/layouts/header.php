<?php

use backend\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<header class="main-header">
	<?= Html::a(Yii::$app->name, Yii::$app->homeUrl . '/../', ['class' => 'logo', 'target' => '_blank']) ?>

	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>

		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-header">
							<p>
								<?= Yii::$app->user->identity->username ?>
								<small>Зарегистрирован: <b><?= Yii::$app->formatter->asDateTime(Yii::$app->user->identity->created_at, Yii::$app->formatter->dateFormat) ?></b> <small><?= Yii::$app->formatter->asDateTime(Yii::$app->user->identity->created_at, Yii::$app->formatter->timeFormat) ?></small></small>
							</p>
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat">Профиль</a>
							</div>
							<div class="pull-right">
								<?= Html::a(
									'Выход',
									['/logout'],
									['data-method' => 'post','class'=>'btn btn-default btn-flat']
								) ?>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
