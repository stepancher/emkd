<?php
    use yii\bootstrap\Nav;
    use yii\helpers\Url;
    use yii\helpers\Html;
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class=" <?php if (in_array(Yii::$app->requestedRoute, ['payments','payments/index','payments/view'])) : ?>active<?php endif; ?>">
                <a href="<?= Url::toRoute(['/personal/payments']) ?>">
                    <i class="fa fa-file"></i> <span>Платежи</span>
                </a>
            </li>
        </ul>

        <hr style="margin:0;"/>

    </section>

</aside>
