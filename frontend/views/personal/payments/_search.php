<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payments-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date_add') ?>

    <?= $form->field($model, 'date_pay') ?>

    <?= $form->field($model, 'date_enrolled') ?>

    <?= $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'num_bank') ?>

    <?php // echo $form->field($model, 'summ') ?>

    <?php // echo $form->field($model, 'payment_info1') ?>

    <?php // echo $form->field($model, 'payment_info2') ?>

    <?php // echo $form->field($model, 'payment_info3') ?>

    <?php // echo $form->field($model, 'payment_info4') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'acc_for') ?>

    <?php // echo $form->field($model, 'hash') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('payments', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('payments', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
