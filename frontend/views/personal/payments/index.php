<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('payments', 'Payments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-index">

    <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'date_add',
                    'date_pay',
                    'date_enrolled',
                    'type:ntext',
                     'num_bank:ntext',
                     'summ',
                    // 'payment_info1:ntext',
                    // 'payment_info2:ntext',
                    // 'payment_info3:ntext',
                    // 'payment_info4:ntext',
                     'status',
                    // 'acc_for',
                    // 'hash:ntext',
                    // 'created_at',
                    // 'updated_at',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                    ]
                ],
            ]); ?>

        </div>
        <div class="col-sm-2">
        </div>
    </div>
</div>