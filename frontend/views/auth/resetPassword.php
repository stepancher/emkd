<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */


$this->title = \Yii::t('auth','Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-box">
    <div class="login-logo"><?= Html::encode($this->title) ?></div>
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <?= $form->field($model, 'password', [
            'template' => "{label}<div class='form-group has-feedback'>{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span></div>\n\n{hint}\n{error}"
        ])->passwordInput() ?>
        <div class="row">
            <div class="col-xs-12">
                <?= Html::submitButton('Войти', ['class' => 'pull-right btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>