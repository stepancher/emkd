<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */


$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-box">
    <div class="login-logo"><?= Html::encode($this->title) ?></div>
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <?= $form->field($model, 'email', [
            'template' => "{label}<div class='form-group has-feedback'>{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span></div>\n\n{hint}\n{error}"
        ]) ?>
        <?= $form->field($model, 'password', [
            'template' => "{label}<div class='form-group has-feedback'>{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span></div>\n\n{hint}\n{error}"
        ])->passwordInput() ?>
        <?php
        $bad_try_input_ip = \Yii::$app->cache->get('bad_try_input_ip_'.\Yii::$app->request->getUserIP());
        $bad_try_input_email = \Yii::$app->cache->get('bad_try_input_email_'.$model->email);
        ?>
        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <div class="col-xs-4">
                <?= Html::submitButton('Войти', ['class' => 'pull-right btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                Вы также можете
                <?= Html::a('Восстановить пароль','request-password-reset') ?>
                или
                <?= Html::a('Зарегистрироваться','signup') ?>

            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>