<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Зарегистрироваться';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-box">
    <div class="login-logo">Signup</div>
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <?= $form->field($model, 'username', [
            'template' => "{label}<div class='form-group has-feedback'>{input}<span class='glyphicon form-control-feedback'></span></div>\n\n{hint}\n{error}"
        ]) ?>
        <?= $form->field($model, 'email', [
            'template' => "{label}<div class='form-group has-feedback'>{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span></div>\n\n{hint}\n{error}"
        ]) ?>
        <?= $form->field($model, 'password', [
            'template' => "{label}<div class='form-group has-feedback'>{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span></div>\n\n{hint}\n{error}"
        ])->passwordInput() ?>

        <?= $form->field($model, 'passwordConfirm', [
            'template' => "{label}<div class='form-group has-feedback'>{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span></div>\n\n{hint}\n{error}"
        ])->passwordInput() ?>

        <?= $form->field($model, 'is_agree')->checkbox([
            'template' => '{input} {label} '.Html::a(\Yii::t('auth','with rules'),'/rules') .'{error}'
        ]) ?>

        <div class="row">
            <div class="col-xs-12">
                <?= Html::submitButton('Войти', ['class' => 'pull-right btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                Вы также можете
                <?= Html::a('Восстановить пароль','request-password-reset') ?>
                или
                <?= Html::a('Зарегистрироваться','signup') ?>

            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>