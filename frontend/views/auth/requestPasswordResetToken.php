<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */


$this->title = \Yii::t('auth','Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-box">
    <div class="login-logo"><?= Html::encode($this->title) ?></div>
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <?= $form->field($model, 'email', [
            'template' => "{label}<div class='form-group has-feedback'>{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span></div>\n\n{hint}\n{error}"
        ]) ?>
        <div class="row">
            <div class="col-xs-8">
            </div>
            <div class="col-xs-4">
                <?= Html::submitButton('Войти', ['class' => 'pull-right btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                Вы также можете
                <?= Html::a('Зарегистрироваться','signup') ?>

            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>