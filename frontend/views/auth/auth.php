<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $modelLogin \common\models\LoginForm */
/* @var $modelSignup \frontend\models\SignupForm */

$this->title = \Yii::t('auth','Authentication');
$this->params['breadcrumbs'][] = false;
?>

<!--div id="page-auth">
	<div class="row">
		<div class="col-sm-6">
			111
		</div>

		<div class="col-sm-6">
			222
		</div>
	</div>
</div-->


    <div class="row">
        <div class="col-sm-6">
            <h2><p><?= \Yii::t('auth','login'); ?></p></h2>

            <p><?= \Yii::t('auth','fill login'); ?></p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($modelLogin, 'email') ?>
                <?= $form->field($modelLogin, 'password')->passwordInput() ?>
                <?= $form->field($modelLogin, 'rememberMe')->checkbox() ?>

                <?php
                $bad_try_input_ip = \Yii::$app->cache->get('bad_try_input_ip_'.\Yii::$app->request->getUserIP());
                $bad_try_input_email = \Yii::$app->cache->get('bad_try_input_email_'.$modelLogin->email);

                ?>
                <div style="color:#999;margin:1em 0">
                    <?= \Yii::t('auth','if forgot password'); ?> <?= Html::a( \Yii::t('auth','reset it'), ['/request-password-reset']) ?>.
                </div>
                <div class="form-group">
                    <?= Html::submitButton(\Yii::t('auth','login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>





        <div class="col-sm-6">
            <h2><p><?= \Yii::t('auth','Signup'); ?></p></h2>
            <p><?=\Yii::t('auth','Please fill out the following fields to signup:')?></p>
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <?= $form->field($modelSignup, 'username') ?>
            <?= $form->field($modelSignup, 'email') ?>
            <?= $form->field($modelSignup, 'password')->passwordInput() ?>
            <?= $form->field($modelSignup, 'passwordConfirm')->passwordInput() ?>
            <?= $form->field($modelSignup, 'is_agree')->checkbox([
                'template' => '{input} {label} '.Html::a(\Yii::t('auth','with rules'),'/rules') .'{error}'
            ]) ?>
            <div class="form-group">
                <?= Html::submitButton(\Yii::t('auth','Signup'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
