<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="login-widget">
	<div class="buttons block">
		<?= Html::a('<i class="icon lock"></i>Вход', '#form-modal-signin', ['class'=>'btn-signin btn btn-primary left']); ?>
		<?= Html::a('Регистрация', '#form-modal-signup', ['class'=>'btn-signup right']); ?>
	</div>

</div>