<?php
use \yii\helpers\Html;
?>

<div class="profile-widget">
	<div class="top">
		<?= Html::a(Yii::$app->user->identity->username, '/personal/index', ['class' => 'user-name']); ?>
		<?= Html::a('Выход', '/logout', ['class' => 'btn-logout']); ?>
	</div>

	<!--div class="bottom">
		<big>23 500 руб.</big> + 450 баллов
	</div-->
</div>
