<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = \Yii::t('auth','Signup');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
	<h2><?= Html::encode($this->title) ?></h2>

	<?php $form = ActiveForm::begin(['id' => 'form-signup','action'=>'/auth']); ?>
		<div class="content">
			<p><?=\Yii::t('auth','Please fill out the following fields to signup:')?></p>

			<?= $form->field($model, 'username') ?>
			<?= $form->field($model, 'email') ?>
			<?= $form->field($model, 'password')->passwordInput() ?>
			<?= $form->field($model, 'passwordConfirm')->passwordInput() ?>
			<?= $form->field($model, 'is_agree')->checkbox([
				'template' => '{input} {label} '.Html::a(\Yii::t('auth','with rules'),'/rules') .'{error}'
			]) ?>
		</div>
		<div class="bottom">
			<?= Html::submitButton(\Yii::t('auth','Signup'), ['class' => 'btn btn-big btn-primary', 'name' => 'signup-button']) ?>
		</div>
	<?php ActiveForm::end(); ?>
</div>
