<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

?>
<div class="site-login">
	<h2><?= \Yii::t('auth','login'); ?></h2>

	<?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'action'=>'/auth']); ?>
		<div class="content">
			<p><?= \Yii::t('auth','fill login'); ?></p>

			<?= $form->field($model, 'email') ?>
			<?= $form->field($model, 'password')->passwordInput() ?>
			<?= $form->field($model, 'rememberMe')->checkbox() ?>

			<p><?= \Yii::t('auth','if forgot password'); ?> <?= Html::a( \Yii::t('auth','reset it'), ['/request-password-reset'], ['class' => 'link-dashed']) ?>. <?= \Yii::t('auth','or'); ?> <?= Html::a( \Yii::t('auth','registration'), ['/auth'], ['class' => 'link-dashed']) ?></p>
		</div>
		<div class="bottom">
			<?= Html::submitButton(\Yii::t('auth','login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
		</div>
	<?php ActiveForm::end(); ?>
</div>
