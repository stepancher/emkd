<?php

return [
	'' => 'site/index',
    'login'=>'auth/login',
    'signup'=>'auth/signup',
    'logout'=>'auth/logout',
    'request-password-reset'=>'auth/request-password-reset',
    'reset-password'=>'auth/reset-password',
    'email-confirm'=>'auth/email-confirm',
    '/personal/payments'=>'/personal/payments/index',
    '/personal/payments/'=>'/personal/payments/index'

//    'personal'=>'personal/index',
//    'personal/<action>'=>'personal/<action>',
];
