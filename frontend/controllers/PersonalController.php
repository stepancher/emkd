<?php
namespace frontend\controllers;

use common\models\City;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;

use common\models\User;
use common\models\UserProfile;
use frontend\models\UserForm;

use vova07\fileapi\actions\UploadAction as FileAPIUpload;

/**
 * Class PersonalController
 * @package frontend\controllers
 */
class PersonalController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }



    public function actionIndex()
    {
        return $this->render('index');

    }
}