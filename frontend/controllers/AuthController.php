<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\emailConfirmForm;
use frontend\models\SignupForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Class AuthController Авторизация, регистрация, подтверждение пароля, восстановление пароля
 * @package frontend\controllers
 */
class AuthController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','logout','reset-password','email-confirm','request-password-reset','signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Страница авторизации на сайте
     */
    public function actionLogin(){
        $model = new LoginForm();
        if(Yii::$app->request->post()) {
            if ($model->load(Yii::$app->request->post()) && $model->login()) { //загружаем данные пост, и пробуем залогиниться
                return $this->goBack();
            } else {
                if ($model->checkNoActivateUser()) {
                    Yii::$app->session->setFlash('error', \Yii::t('auth', 'Confirm your email'));
                    return $this->goBack();
                }
            }
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            $key ='user_last_signup_'.\Yii::$app->request->getUserIP();
            if(\Yii::$app->cache->exists($key) and \Yii::$app->cache->get($key)+60*2>time()) {
                Yii::$app->getSession()->setFlash('success', \Yii::t('auth', 'you can\'t signup more than 1 time in 2 minutes'));
                return $this->goBack();
            }else{
                \Yii::$app->cache->delete($key); //удаляем из мемкеша
            }
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    Yii::$app->getSession()->setFlash('success', \Yii::t('auth', 'Check your email for further instructions.'));
                    return $this->goBack();
                }
            }
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }



    /**
     * Форма восстановления пароля
     * @return string|\yii\web\Response
     */
    public function actionRequestPasswordReset()
    {
        $key ='user_last_reset_pass_'.\Yii::$app->request->getUserIP();
        //Проверка на повторность запроса сброса пароля менее чем за 10 минут
        if(!\Yii::$app->cache->exists($key) or \Yii::$app->cache->get($key)+60*10<time()) {
            \Yii::$app->cache->delete($key); //удаляем из мемкеша
            $model = new PasswordResetRequestForm();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) { //приняли данные пишем в модель валидируем
                if ($model->sendEmail()) { //отправляем письмо
                    Yii::$app->getSession()->setFlash('success', \Yii::t('auth', 'Check your email for further instructions.'));
                    return $this->goHome();
                } else {
                    Yii::$app->getSession()->setFlash('error', \Yii::t('auth', 'Sorry, we are unable to reset password for email provided.'));
                }
            }

            return $this->render('requestPasswordResetToken', [
                'model' => $model,
            ]);
        }else{
            Yii::$app->getSession()->setFlash('success', \Yii::t('auth', 'you can\'t request a password reset more than 1 time in 10 minutes'));
            return $this->goHome();
        }
    }

    /**
     * @param $token токен восстановления пароля
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', \Yii::t('auth','New password was saved.'));
            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**При переходе по ссылке в письме, пользователь переходит сюда
     * @param $token
     * @return \yii\web\Response
     */
    public function actionEmailConfirm($token)
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException(\Yii::t('auth','Email confirm token cannot be blank.'));
        }

        $user = User::findByEmailConfirmToken($token);
        if($user) {
            if (User::isEmailConfirmTokenValid($token)) { //токен валидный по времени
                if (!$user->isActive()) { //
                    if ($user->confirmEmail()) {
                        Yii::$app->getSession()->setFlash('success', \Yii::t('auth', 'Your email was confirm'));
                    }
                } else {
                    Yii::$app->getSession()->setFlash('success', \Yii::t('auth', 'Your address has been confirmed previously'));
                }
            } else {  //токен просрочен высылаем заново
                $user->generateEmailConfirmToken();
                if ($user->save()) {
                    SignupForm::sendEmailConfirm($user);
                    Yii::$app->getSession()->setFlash('success', \Yii::t('auth', 'Invalid email confirm token. We resend confirm email, check it'));
                }
            }
        }else
        {
            Yii::$app->getSession()->setFlash('success', \Yii::t('auth', 'Invalid confirm token'));
        }
        return $this->goHome();
    }
}















