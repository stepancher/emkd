<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\UserProfile;
use yii\base\Event;
use vendor\exru\mailnotify\models;
use vendor\exru\mailnotify\models\MailNotify;
/**
 * Форма регистрации
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $passwordConfirm;
    public $is_agree;


    public function rules()
    {
        return [

            [['username', 'email','password','passwordConfirm'], 'filter', 'filter' => 'trim'],
            [['username', 'email','password','passwordConfirm'], 'filter', 'filter' => 'strip_tags'],
            [['username','email','password','passwordConfirm'], 'required', 'message'=>\Yii::t('auth', 'can\'t be empty')],
            ['username', 'string', 'min' => 2, 'max' => 100, 'tooShort' => \Yii::t('auth', 'minimum character',['n'=>2]), 'tooLong' => \Yii::t('auth', 'maximum character',['n'=>255])],

            ['email', 'email', 'message'=>\Yii::t('auth', 'incorrect email')],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => \Yii::t('auth', 'This email address has already been taken.')],

            [['password','passwordConfirm'], 'string', 'min' => 6, 'tooShort' => \Yii::t('auth', 'minimum character',['n'=>6])],
            [['passwordConfirm'], 'compare', 'compareAttribute' => 'password', 'message' => \Yii::t('auth', 'The passwords do not match')],

	        ['is_agree', 'required', 'requiredValue' => 1, 'message' => \Yii::t('auth', 'Please agree with rules')],
        ];
    }

    /**
     * Регистрация пользователя
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateEmailConfirmToken(); //создаем токен потверждения

            if ($user->save()) {
                $userRole = Yii::$app->authManager->getRole('user'); //устанавливаем дефолтную роль юзера
                Yii::$app->authManager->assign($userRole, $user->getId());

                $userProfile = new UserProfile();
                $userProfile->firstname = $this->username;
                $userProfile->sex = '1';
                $userProfile->id_user = $user->getId();

                if (!$userProfile->save()) {
                    return null;
                }
                \Yii::$app->cache->set('user_last_signup_'.\Yii::$app->request->getUserIP(),time()); //пишем в мемкеш время последней удачной регистрации


                $this->sendEmailConfirm($user);

                return $user;
            }
        }
        return null;
    }

    public static function sendEmailConfirm($user)
    {
        if ($user) {
            return \Yii::$app->mailer->compose(['html' => 'emailConfirmToken-html', 'text' => 'emailConfirmToken-text'], ['user' => $user])
                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($user->email)
                ->setSubject('Email confirm for ' . \Yii::$app->name)
                ->send();
        }
        return false;
    }

    public function attributeLabels()
    {
        return [
            'username' => \Yii::t('auth','username'),
            'email' =>\Yii::t('auth','email'),
            'password' => \Yii::t('auth','password'),
            'passwordConfirm' => \Yii::t('auth','Password confirm'),
            'is_agree' => \Yii::t('auth','is_agree'),
        ];
    }
}
