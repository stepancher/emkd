<?php
namespace frontend\widgets;

use yii;
use yii\base\Widget;
use frontend\models\SignupForm as FormSignUp;

class SignUpForm extends Widget{

	public function run() {
            $model = new FormSignUp();
            return $this->render('@frontend/views/widgets/SignUpForm', [
                'model' => $model,
            ]);
	}
}