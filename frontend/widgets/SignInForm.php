<?php
namespace frontend\widgets;

use yii;
use yii\base\Widget;
use common\models\LoginForm;
class SignInForm extends Widget{

	public function run() {
        $model = new LoginForm();
        return $this->render('@frontend/views/widgets/SignInForm', [
            'model' => $model,
        ]);
	}
}