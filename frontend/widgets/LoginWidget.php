<?php
namespace frontend\widgets;

use yii;
use yii\base\Widget;

class LoginWidget extends Widget{
    
    public function run() {
	    $tmp = !Yii::$app->user->isGuest ? 'login':'nologin';
        return $this->render('@frontend/views/widgets/LoginWidget/' . $tmp);
    }
}